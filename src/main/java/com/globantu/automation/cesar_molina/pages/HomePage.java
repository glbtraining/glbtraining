package com.globantu.automation.cesar_molina.pages;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.globantu.automation.cesar_molina.utils.TestUtils;

public class HomePage {

	WebDriver driver;

	@FindBy(id = "flight-origin")
	private WebElement flightOrigin;

	@FindBy(id = "flight-destination")
	private WebElement flightDestination;

	@FindBy(id = "package-origin")
	private WebElement packageOrigin;

	@FindBy(id = "package-destination")
	private WebElement packageDestination;

	@FindBy(id = "flight-departing")
	private WebElement flightDeparting;

	@FindBy(id = "package-departing")
	private WebElement packageDeparting;

	@FindBy(id = "flight-returning")
	private WebElement flightReturning;

	@FindBy(id = "package-returning")
	private WebElement packageReturning;

	@FindBy(id = "tab-flight-tab")
	private WebElement tabFlightButton;

	@FindBy(id = "tab-package-tab")
	private WebElement tabFlightHotelButton;

	@FindBy(id = "flight-type-roundtrip-label")
	private WebElement roundtrip;

	@FindBy(id = "tab-package-tab")
	private WebElement tabPackageButton;

	@FindBy(id = "tab-hotel-tab")
	private WebElement tabHotelButton;

	@FindBy(id = "flight-adults")
	private WebElement packageAdults;

	@FindBy(id = "search-button")
	private WebElement searchButton;

	@FindBy(name = "sort")
	private WebElement sorterDrop;

	@FindBy(xpath = "//div[2]/div/button")
	private WebElement selectFlight;

	@FindBy(xpath = "//div[2]/div/button")
	private WebElement departureToLosAngeles;

	@FindBy(xpath = "//li[3]/div[2]/div/div[2]/div/button")
	private WebElement departureToLasVegas;

	@FindBy(xpath = "//button[2]")
	private WebElement nextButton;

	@FindBy(xpath = "//tr[3]/td[7]/button")
	private WebElement middleDay;

	@FindBy(xpath = "//tr[2]/td[1]/button")
	private WebElement departingDay;

	@FindBy(xpath = "//tr[3]/td[6]/button")
	private WebElement returningDay;

	@FindBy(xpath = "//div/li/a/div")
	private WebElement suggestedOption;

	@FindBy(xpath = "//span/button")
	private WebElement continueButton;

	@FindBy(id = "continue-booking")
	private WebElement continueBookingButton;

	@FindBy(id = "firstname[0]")
	private WebElement firstNameField;

	@FindBy(id = "country_code[0]")
	private WebElement countryCode;

	@FindBy(id = "forcedChoiceNoThanks")
	private WebElement noThanksButton;

	@FindBy(id = "partialHotelBooking")
	private WebElement partStayCheck;

	@FindBy(id = "package-rooms")
	private WebElement roomsDrop;

	@FindBy(xpath = "//li[2]/button")
	private WebElement priceOption;

	@FindBy(id = "resultsContainer")
	private WebElement resultsContainer;

	@FindBy(xpath = "//article[2]/div/div/div[2]/div[2]/a")
	private WebElement firstRoom;

	private String URL = "https://www.travelocity.com/";
	private String shortestDuration = "Duration (Shortest)";


  private WebDriverWait wait = null;
	
	public void go(WebDriver driver) {
		this.driver = driver;
		this.driver.get(URL);
	      wait = new WebDriverWait(this.driver, 20);
	}

	public void searchFlight(String origin, String destination){
	  tabFlightButton.click();
      roundtrip.click();

      // Select Origin & Destination
      wait.until(ExpectedConditions.elementToBeClickable(flightOrigin));
      flightOrigin.clear();
      new Actions(driver).moveToElement(flightDestination).perform();
      flightOrigin.sendKeys(origin);
      wait.until(ExpectedConditions.elementToBeClickable(suggestedOption));
      suggestedOption.click();

      flightDestination.clear();
      new Actions(driver).moveToElement(flightDestination).perform();
      flightDestination.sendKeys(destination);
      wait.until(ExpectedConditions.elementToBeClickable(suggestedOption));
      suggestedOption.click();

      // Choose the Date
      flightDeparting.click();
      nextButton.click();
      nextButton.click();
      nextButton.click();
      middleDay.click();

      flightReturning.click();
      nextButton.click();
      middleDay.click();

      searchButton.click();
	}
	
	public void testCase1() {
		wait.until(ExpectedConditions.elementToBeClickable(tabFlightButton));
		

		// Sort by shorest duration
		wait.until(ExpectedConditions.elementToBeClickable(sorterDrop));
		// Verifying Result Page
		Assert.assertTrue(TestUtils.verifyText(driver, "Select your departure to Los Angeles"));
		Assert.assertTrue(sorterDrop.isDisplayed());
		new Select(sorterDrop).selectByVisibleText(shortestDuration);

		// Select the 1st option to Los Angeles
		wait.until(ExpectedConditions.elementToBeClickable(departureToLosAngeles));
		departureToLosAngeles.click();

		// Sort by shorest duration
		wait.until(ExpectedConditions.elementToBeClickable(sorterDrop));
		Assert.assertTrue(TestUtils.verifyText(driver, "Select your return to Las Vegas"));
		Assert.assertTrue(sorterDrop.isDisplayed());
		new Select(sorterDrop).selectByVisibleText(shortestDuration);

		// Select the 3rd option to Las Vegas
		wait.until(ExpectedConditions.elementToBeClickable(departureToLasVegas));
		departureToLasVegas.click();

		try {
			wait.until(ExpectedConditions.elementToBeClickable(noThanksButton));
			noThanksButton.click();
		} catch (Exception e) {

		}

		// Change focus to new tab
		ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(newTab.get(1));

		// Verifying Trip Page
		Assert.assertTrue(TestUtils.verifyText(driver, "Review your trip"));
		Assert.assertTrue(TestUtils.verifyText(driver, "Trip Summary"));
		Assert.assertTrue(TestUtils.verifyText(driver, "Important Flight Information"));
		Assert.assertTrue(TestUtils.verifyText(driver, "Trip Total"));
		Assert.assertTrue(continueButton.isDisplayed());

		continueButton.click();

		// Verifying Payments Page
		Assert.assertTrue(TestUtils.verifyText(driver, "Who's traveling?"));
		Assert.assertTrue(firstNameField.isDisplayed());
		Assert.assertTrue(countryCode.isDisplayed());
		Assert.assertTrue(TestUtils.verifyText(driver, "Payment"));
		Assert.assertTrue(TestUtils.verifyText(driver, "Las Vegas to Los Angeles"));
		Assert.assertTrue(continueBookingButton.isDisplayed());
	}

	public void testCase2(String origin, String destination) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(tabPackageButton));

		tabPackageButton.click();

		// Verifying Result Page
		Assert.assertTrue(roomsDrop.isDisplayed());
		Assert.assertTrue(partStayCheck.isDisplayed());

		// Select Origin & Destination
		wait.until(ExpectedConditions.elementToBeClickable(packageOrigin));
		packageOrigin.clear();
		new Actions(driver).moveToElement(packageDestination).perform();
		packageOrigin.sendKeys(Keys.ARROW_DOWN);
		packageOrigin.sendKeys(origin);

		try {
			wait.until(ExpectedConditions.elementToBeClickable(suggestedOption));
			suggestedOption.click();
		} catch (Exception e) {

		}

		packageDestination.clear();
		new Actions(driver).moveToElement(packageDestination).perform();
		packageDestination.sendKeys(Keys.ARROW_DOWN);
		packageDestination.sendKeys(destination);

		try {
			wait.until(ExpectedConditions.elementToBeClickable(suggestedOption));
			suggestedOption.click();
		} catch (Exception e) {

		}

		// Choose the Date
		packageDeparting.click();
		nextButton.click();
		departingDay.click();

		packageReturning.click();
		returningDay.click();

		searchButton.click();

		// Sort by lowest price
		wait.until(ExpectedConditions.elementToBeClickable(priceOption));
		priceOption.click();

		List<WebElement> articleOptions = resultsContainer.findElements(By.tagName("article"));
		boolean selectOption = false;

		for (WebElement article : articleOptions) {
			List<WebElement> hotelNames = article.findElements(By.className("visuallyhidden"));
			int i = 0;
			for (WebElement hotel : hotelNames) {
				String value = hotel.getText();

				if (i == 0) {
					i++;
				}

				if (value.equals("Sheraton Agoura Hills Hotel")) {
					break;
				} else {
					if (value.contains("out of 5.0")) {
						double starts = Double.parseDouble(value.substring(0, 3));
						if (starts >= 3.0) {
							selectOption = true;
							article.click();
						}
						break;
					}
				}
				if (selectOption) {
					break;
				}
			}

			if (selectOption) {
				break;
			}
		}

		ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		if (newTab.size() > 1)
			driver.switchTo().window(newTab.get(1));

		wait.until(ExpectedConditions.elementToBeClickable(firstRoom));
		firstRoom.click();
	}

	public void testCase3(String origin, String destination) {
		tabHotelButton.click();
		flightOrigin.clear();
		flightOrigin.sendKeys(origin);
		flightDestination.clear();
		flightDestination.sendKeys(destination);

		flightDeparting.sendKeys("01/17/2017");
		flightReturning.sendKeys("01/18/2017");

		packageAdults.click();
	}
	
	//Re-factor
	private void giveFocus(WebElement element){
	  new Actions(driver).moveToElement(element).perform();
	}
	
	private void sendKeysFlightOrigin(String keys){
	  flightOrigin.sendKeys(keys);
	}
}
