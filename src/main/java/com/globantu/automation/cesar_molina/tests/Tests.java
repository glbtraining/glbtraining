package com.globantu.automation.cesar_molina.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import org.testng.annotations.Parameters;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

import com.globantu.automation.cesar_molina.pages.HomePage;
import com.globantu.automation.cesar_molina.utils.TestUtils;

public class Tests {


  private String origin = "LAS";
  private String destination = "LAX";
  static final String KEY = "webdriver.chrome.driver";
  // String key = "webdriver.firefox.driver";
  static final String PATH = "C:/Users/cesar.molina/Documents/Globant/Tools/drivers/chromedriver.exe";
  // String path =
  // "C:\\Users\\cesar.molina\\Documents\\Globant\\Tools\\drivers\\chromedriver.exe";

  WebDriver driver;

  @BeforeMethod
  public void setup() {
    System.setProperty(KEY, PATH);
    driver = new ChromeDriver();
  }

  @AfterMethod
  public void tearDown() {
    driver.close();
    driver.quit();
  }


  @Test
  // @Parameters({"origin", "destination"})
  public void testCase1() {
    System.out.println("Start");
    HomePage homePage = PageFactory.initElements(driver, HomePage.class);
    homePage.go(driver);
    homePage.searchFlight(origin, destination);
    homePage.testCase1();
  }

  @Test
  public void testCase2() {
    HomePage homePage = PageFactory.initElements(driver, HomePage.class);
    homePage.go(driver);
    homePage.testCase2(origin, destination);
  }

  @Test
  public void testDate() {
    TestUtils.addMonths(2);
  }

}
