package com.globantu.automation.cesar_molina.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TestUtils {
	
	public static Date addMonths(int amount){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		System.out.println("Current date: " + dateFormat.format(date));
		date = DateUtils.addMonths(date, amount);
		date.getTime();
		System.out.println("Wanted date: " + dateFormat.format(date));
		Calendar myCal = new GregorianCalendar();
	    myCal.setTime(date);

	    System.out.println("Day: " + myCal.get(Calendar.DAY_OF_MONTH));
	    System.out.println("Month: " + (myCal.get(Calendar.MONTH) + 1));
	    System.out.println("Year: " + myCal.get(Calendar.YEAR));
		return date;
	}
	
	public static boolean verifyText(WebDriver driver, String text){
		return driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*" + text + "[\\s\\S]*$");
	}
}
